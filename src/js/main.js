'use strict'

document.addEventListener('DOMContentLoaded', () => {
  let allTracks = document.querySelectorAll('.fairy-tales-list__track'),
      allSounds = document.querySelectorAll('audio'),
      allButtons = document.querySelectorAll('.button')

  // Воспроизводит все аудиозаписи
  let mainButton = document.querySelector('.button_main a')

  mainButton.addEventListener('click', (event) => {
    event.preventDefault()

    loadmusic(0)

    function loadmusic(index) {
      let firstSound = index, // С первого трека
          mainImage = mainButton.querySelector('img'),
          src = mainImage.getAttribute('src') == '/image/icon/play-main.svg' ? '/image/icon/pause-main.svg' : '/image/icon/play-main.svg'

      if (allSounds[index].paused == false) {
        allSounds[index].pause()
      } else {
        allSounds[index].play()
      }

      mainImage.src = src

      firstSound++ // готовим следущий трек

      if (firstSound > allSounds.lenght) { // если треков не осталось, то останавливаем проигрывание
        allSounds[index].pause()
        allSounds.currentTime = 0.0
      }

      allSounds[index].addEventListener('ended', () => {
        setTimeout(loadmusic(firstSound), allSounds[index].duration)
      })
    }
  })

  // Воспроизводит отдельные аудиозаписи
  allTracks.forEach((track, index) => {
    let button = track.querySelector('.button a')

    button.addEventListener('click', (event) => {
      event.preventDefault()

      let image = button.querySelector('img'),
          src = image.getAttribute('src') == '/image/icon/play.svg' ? '/image/icon/pause.svg' : '/image/icon/play.svg',
          duration = track.nextElementSibling.querySelector('span')

      // Переключает состояние плеера и меняет иконку на кнопке (плей или пауза)
      if (allSounds[index].paused == false) {
        allSounds[index].pause()
      } else {
        // Приостанавливает все включенные ранее записи
        for (let i = 0; i < allSounds.length; i++) {
          allSounds[i].pause()

          allButtons.forEach(button => {
            button.querySelector('a img').src = '/image/icon/play.svg'
          })
        }

        allSounds[index].play()
      }

      image.src = src

      // По окончании трека устанавливает на кнопке иконку плей
      allSounds[index].addEventListener('ended', () => {
        image.src = '/image/icon/play.svg'
      })

      // Обновляет текущую позицию воспроизведения
      allSounds[index].addEventListener('timeupdate', () => {
        let time = new Date(allSounds[index].currentTime * 1000),
            width = 0

        width = (time.getSeconds() / allSounds[index].duration * 100).toFixed(2) + '%'

        duration.style.width = width
      })
    })
  })
})
